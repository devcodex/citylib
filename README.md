# CityLib - Library Application

This application and microservices serve as the basis for the CityLib application.
This application stores and displays informations such as:
* Book information
* User information
* Library information
* Rents and returns of books information
    
## Project structure

* Webapp Application
    * Main application for CityLib, contains front end application as well as accounts.
    
* Books Microservice
    * Contains Book, Author and Publisher information.
    * List of APIs available at http://localhost:8081/swagger-ui.html when service is running
    
* Library Microservice
    * Library information.
    * All information relating to book copies (Availability, copy information, etc...).
    * Rented copies information.
    * List of APIs available at http://localhost:8082/swagger-ui.html when service is running
     
* BatchMailer Microservice
    * Responsible for mailing tasks.
    * Contains batch task for alert emails.
    * Launches a scheduled operation once a day at 17:00h, that sends emails for each book past due date of return
    * List of APIs available at http://localhost:8083/swagger-ui.html when service is running

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

Java 8

A Java IDE

Maven - if you don't have it installed you can install
         it by clicking [here](https://maven.apache.org/download.cgi).
         
Activated Annotation Processing on your IDE, to see how to Activate Annotation Processing [click here](https://immutables.github.io/apt.html).

Git (if you wish to clone or fork the repository)

A PostgreSQL server running on your local machine (Recommended version 4 or higher)

### Installing

1. Start by retrieving the project from the current repository, to do this, simply clone the project,
or download the project to your local machine.
 
2. Open your IDE and start a new project from existing sources. Make sure to import the project as a Maven project with for projects recursively active (this ensures each module is imported correctly).

3. On your PostGreSQL server create the following databases:
    * citylib-books
    * citylib-library
    * citylib-accounts

4. Follow the instructions on each of the following files:
    * citylib/books/src/main/resources/application.properties
    * citylib/library/src/main/resources/application.properties
    * citylib/webapp/src/main/resources/application.properties
 
5. Open a terminal/console on base folder of each module (webapp, library, books and batchmailer) and execute the
following command:
````maven install````
This will download and install all necessary dependencies for the project to run.

6. This project uses Lombok to auto generate Getters and Setters 
(Lombok has many more uses that are not fully explored in this project),
and for it's proper functioning it is required that you have Annotation Processing activated in your IDE.

7. To generate the initial tables and data for each database, run each of the following configurations once:
    * webapp
    * books
    * library
    
8. After the previous services have finished launching, inside each application.properties comment or delete the
line "spring.datasource.initialization-mode=always".

9. The project should now be up and running.

10. You can create an account or use one the accounts provided on the "Additional information" section bellow. 

## Additional information

Tables and initial data is loaded automatically if the properties files were modified correctly and the necessary databases were created.

The following 5 users can be used for testing purposes (They should be removed when application goes live):
* user1
    * password: password
    * email: citylib_user1@yopmail.com
* user2
    * password: password
    * email: citylib_user2@yopmail.com
* user3
    * password: password
    * email: citylib_user3@yopmail.com
* user4
    * password: password
    * email: citylib_user4@yopmail.com
* user5
    * password: password
    * email: citylib_user5@yopmail.com
        
Yopmail is a free disposable mail service that requires no password in order to view or receive emails.

## Built With
[Maven](https://maven.apache.org/) - Dependency Management

## Authors

**Bruno Ferreira**