package com.citylib.batchmailer.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DefaultConfig {
    @Bean
    public ServiceRoutes serviceRoutes() {
        return new ServiceRoutes();
    }
}
