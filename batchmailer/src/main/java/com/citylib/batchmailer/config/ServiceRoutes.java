package com.citylib.batchmailer.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

@Getter
@PropertySource("classpath:application.properties")
public class ServiceRoutes {
    @Value("${service.book}")
    private String books;

    @Value("${service.library}")
    private String library;

    @Value("${service.webapp}")
    private String webapp;

    public String getLibraryDueTomorrow() {
        return library + "batchOps/dueTomorrow";
    }

    public String getLibraryPastDueDate() {
        return library + "batchOps/pastDueDate";
    }

    public String getBookById(long id) {
        return books + id;
    }

    public String getUser(long id) {
        return webapp + id;
    }
}
