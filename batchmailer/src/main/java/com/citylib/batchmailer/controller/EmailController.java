package com.citylib.batchmailer.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;

@RestController
@RequestMapping("/api/emailbatch")
public class EmailController {
    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    private MailProperties mailProperties;

    private static final Logger logger = LoggerFactory.getLogger(EmailController.class);

    // End point meant to send confirmation email for user upon registration
    @GetMapping("/confirmAccount")
    public ResponseEntity<String> sendConfirmationEmail(
            @RequestParam("authString") String authString,
            @RequestParam("recipient") String recipient
            ) {
        String subject = "Email de confirmation de compte pour CityLib";
        String emailBody = "Merci d'avoir créé un compte avec nous.<br>" +
                "Activez votre compte <a href='http://localhost:8080/accountConfirmation/" + authString + "'>ici</a>.<br>" +
                "<br> Nos meilleures salutations,<br>L'équipe de Citylib";
        return sendMail(mailProperties.getUsername(), recipient, subject, emailBody);
    }

    // Builds and sends confirmation email
    private ResponseEntity<String> sendMail(String fromEmail, String toEmail, String subject, String body) {
        try {
            logger.info("Sending Email to {}", toEmail);
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper messageHelper = new MimeMessageHelper(message, StandardCharsets.UTF_8.toString());
            messageHelper.setSubject(subject);
            messageHelper.setText(body, true);
            messageHelper.setFrom(fromEmail);
            messageHelper.setTo(toEmail);

            mailSender.send(message);
            return new ResponseEntity<>("true", HttpStatus.OK);
        } catch (MessagingException ex) {
            logger.error("Failed to send email to {}", toEmail);
            return new ResponseEntity<>("false", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
