package com.citylib.batchmailer.dto;

import lombok.Data;

@Data
public class BookCopy {
    private long id;
    private long bookId;
}
