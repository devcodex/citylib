package com.citylib.batchmailer.dto;

import lombok.Data;

@Data
public class RentedCopy {
    private long id;
    private long userId;
    private String startDate;
    private boolean extended;
    private boolean returned;
    private BookCopy bookCopy;
}
