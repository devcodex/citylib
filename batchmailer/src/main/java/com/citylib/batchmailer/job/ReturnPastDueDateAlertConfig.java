package com.citylib.batchmailer.job;

import org.quartz.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
public class ReturnPastDueDateAlertConfig {
    @Value("${scheduler.timer.returnPastDueDateAlertTime}")
    private String cronTime;

    // Builds job for batch sending emails with past due date alert
    @Bean
    public JobDetail returnPastDueDateAlertDetails() {
        return JobBuilder.newJob(ReturnPastDueDateAlertJob.class).withIdentity("pastDueDateJob", "CityLibGroup").storeDurably().build();
    }

    // Trigger for the job, takes schedule time value from configuration file
    @Bean
    public Trigger returnPastDueDateAlertTrigger(JobDetail returnPastDueDateAlertDetails) {
        return TriggerBuilder.newTrigger()
                .withIdentity("fireForPastDueDateAlerts", "CityLibGroup")
                .withSchedule(CronScheduleBuilder.cronSchedule(cronTime))
                .forJob(returnPastDueDateAlertDetails)
                .build();
    }
}
