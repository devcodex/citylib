package com.citylib.batchmailer.job;

import com.citylib.batchmailer.config.ServiceRoutes;
import com.citylib.batchmailer.dto.Book;
import com.citylib.batchmailer.dto.RentedCopy;
import com.citylib.batchmailer.dto.User;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Component
public class ReturnPastDueDateAlertJob implements Job {
    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    private MailProperties mailProperties;
    @Autowired
    private ServiceRoutes serviceRoutes;

    private static final Logger logger = LoggerFactory.getLogger(ReturnPastDueDateAlertJob.class);
    private ObjectMapper objectMapper = new ObjectMapper();
    private RestTemplate restTemplate = new RestTemplate();

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        try {
            sendReturnPastDueDateAlertMail();
        } catch (IOException e) {
            logger.error("Return alert batch for pastDueDate failed");
        }
    }

    public void sendReturnPastDueDateAlertMail() throws IOException {
        // Retrieves list of rented copies past due date from library microservice
        ResponseEntity<String> responseDueTomorrow = restTemplate.getForEntity(serviceRoutes.getLibraryPastDueDate(), String.class);
        List<RentedCopy> pastDueDateList = objectMapper.readValue(responseDueTomorrow.getBody(), new TypeReference<List<RentedCopy>>(){});

        // For each rented copy builds the information to be sent in the alert email
        for(RentedCopy rentedCopy : pastDueDateList) {
            Book book = getBook(rentedCopy.getBookCopy().getBookId());
            User user = getUser(rentedCopy.getUserId());

            String subject = "Votre emprunt de " + book.getTitle() + " a depassé la date de retour.";
            StringBuilder body = new StringBuilder();
            body.append("Bonjour ").append(user.getUsername()).append("<br><br>")
                .append("La période d'emprunt pour le livre <b>").append(book.getTitle()).append("</b> est terminée.<br>")
                .append("Nous vous remercions de retourner le livre au plus vite.<br><br>")
                .append("Les retards de retours peuvent entraîner une pénalité.<br><br>")
                .append("Cordialement,<br>L'équipe de Citylib");

            sendMail(mailProperties.getUsername(),user.getEmail(), subject, body.toString());
        }
        logger.info("pastDueDate alert batch has been concluded.");
    }

    // Builds the alert email and sends it
    private void sendMail(String fromEmail, String toEmail, String subject, String body) {
        try {
            logger.info("Sending pastDueDate alert email to {}", toEmail);
            MimeMessage message = mailSender.createMimeMessage();

            MimeMessageHelper messageHelper = new MimeMessageHelper(message, StandardCharsets.UTF_8.toString());
            messageHelper.setSubject(subject);
            messageHelper.setText(body, true);
            messageHelper.setFrom(fromEmail);
            messageHelper.setTo(toEmail);

            mailSender.send(message);
        } catch (MessagingException ex) {
            logger.error("Failed to send pastDueDate alert email to {}", toEmail);
        }
    }

    private Book getBook(long bookId) throws IOException {
        ResponseEntity<String> responseBook = restTemplate.getForEntity(serviceRoutes.getBookById(bookId), String.class);
        Book book = objectMapper.readValue(responseBook.getBody(), Book.class);
        return book;
    }

    private User getUser(long userId) throws IOException {
        ResponseEntity<String> responseBook = restTemplate.getForEntity(serviceRoutes.getUser(userId), String.class);
        User user = objectMapper.readValue(responseBook.getBody(), User.class);
        return user;
    }
}
