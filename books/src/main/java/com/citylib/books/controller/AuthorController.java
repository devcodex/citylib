package com.citylib.books.controller;

import com.citylib.books.dao.AuthorRepository;
import com.citylib.books.model.Author;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@RestController
@RequestMapping("/api/author")
public class AuthorController {
    @Autowired
    AuthorRepository authorRepository;

    Logger logger = LoggerFactory.getLogger(AuthorController.class);

    @GetMapping("/{authorId}")
    public ResponseEntity getAuthor(@PathVariable("authorId") long authorId) {
        Author author = authorRepository.findById(authorId);
        return new ResponseEntity<>(author, HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity postAuthor(@RequestParam("name") String name,
                                     @RequestParam("surname") String surname,
                                     @RequestParam(value = "birthDate", defaultValue = "01/01/1900") String birthDate) {
        Author author = new Author();
        author.setName(name);
        author.setSurname(surname);
        author.setBirthDate(LocalDate.parse(birthDate, DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        authorRepository.save(author);
        logger.info("New author added with id " + author.getId());
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PutMapping("/{authorId}")
    public ResponseEntity putAuthor(@PathVariable("authorId") long authorId,
                                    @RequestParam(value = "name", required = false, defaultValue = "") String name,
                                    @RequestParam(value = "surname", required = false, defaultValue = "") String surname,
                                    @RequestParam(value = "birthDate", required = false, defaultValue = "") String birthDate) {
        Author author = authorRepository.findById(authorId);
        if(!name.equals("")) {
            author.setName(name);
        }
        if(!surname.equals("")) {
            author.setSurname(surname);
        }
        if(!birthDate.equals("")) {
            author.setBirthDate(LocalDate.parse(birthDate, DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        }
        authorRepository.save(author);
        logger.info("Author with id " + author.getId() + " has been modified.");
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @DeleteMapping("/{authorId}")
    public ResponseEntity deleteAuthor(@PathVariable("authorId") long authorId){
        try {
            Author author = authorRepository.findById(authorId);
            authorRepository.delete(author);
            logger.info("Author with id " + author.getId() + " deleted successfully.");
        } catch (DataIntegrityViolationException e) {
            return new ResponseEntity<>("{error: 'This author is still being used by book entities.'}", HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return new ResponseEntity<>("", HttpStatus.OK);
    }
}
