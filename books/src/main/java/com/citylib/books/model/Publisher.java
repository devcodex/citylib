package com.citylib.books.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Publisher {
    //Maison d'edition
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;
}
