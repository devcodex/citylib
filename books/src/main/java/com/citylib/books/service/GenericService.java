package com.citylib.books.service;

import java.util.List;
import javassist.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public interface GenericService<T> {

    List<T> findAll();

    Iterable<T> findAll(Sort sort);

    Page<T> findAll(Pageable pageable);

    T save(T entity);

    void delete(long id);

    void update(T entity);

    T find(long id) throws NotFoundException;

    T edit(T entity);

}
