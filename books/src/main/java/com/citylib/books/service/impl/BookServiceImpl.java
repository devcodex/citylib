package com.citylib.books.service.impl;

import com.citylib.books.dao.BookRepository;
import com.citylib.books.model.Book;
import com.citylib.books.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BookServiceImpl extends GenericServiceImpl<Book> implements BookService {
    private final BookRepository bookRepository;

    @Override
    public JpaRepository<Book, Long> getDao() {
        return bookRepository;
    }

}
