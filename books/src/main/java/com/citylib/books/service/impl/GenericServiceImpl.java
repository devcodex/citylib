package com.citylib.books.service.impl;

import java.util.List;
import java.util.Optional;

import com.citylib.books.service.GenericService;
import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

@Slf4j
public abstract class GenericServiceImpl<T> implements GenericService<T> {

    public abstract JpaRepository<T, Long> getDao();

    @Override
    public List<T> findAll() {
        return getDao().findAll();
    }

    @Override
    public Iterable<T> findAll(Sort sort) {
        return getDao().findAll(sort);
    }

    public Page<T> findAll(Pageable pageable) {
        return getDao().findAll(pageable);
    }

    @Override
    public T save(T entity) {
        return getDao().save(entity);
    }

    @Override
    public void delete(long id) {
        T entity = null;
        try {
            entity = find(id);
            getDao().delete(entity);
        } catch (NotFoundException e) {
            log.error("No entity found to delete with id", id);
        }
    }

    @Override
    public void update(T entity) {
        getDao().save(entity);
    }

    @Override
    public T find(long id) throws NotFoundException {
        Optional<T> entity = getDao().findById(id);
        if (entity.isPresent()) {
            return entity.get();
        } else {
            log.error("Can't find entity id {}", id);
            throw new NotFoundException("No entity with id " + id);
        }
    }

    @Override
    public T edit(T entity) {
        return getDao().saveAndFlush(entity);
    }
}
