package com.citylib.books.controller;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

import static org.junit.Assert.*;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@WebMvcTest(DefaultController.class)
public class DefaultControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getBooksDefault() throws Exception {
        JSONObject jsonObject = new JSONObject(this.restTemplate.getForObject("http://localhost:" + port + "/api/books", String.class));
        JSONArray content = jsonObject.getJSONArray("content");
        assertThat(content.length()).isEqualTo(10);
    }

    @Test
    public void getBooksWith20Size() throws Exception {
        JSONObject jsonObject = new JSONObject(this.restTemplate.getForObject("http://localhost:" + port + "/api/books?size=20", String.class));
        JSONArray content = jsonObject.getJSONArray("content");
        assertThat(content.length()).isEqualTo(20);
    }

    @Test
    public void books()  throws Exception {
        JSONObject jsonObject = new JSONObject(this.restTemplate.getForObject("http://localhost:" + port + "/api/books/1", String.class));
        assertThat(jsonObject.get("id")).isEqualTo(1);
        assertThat(jsonObject.get("title")).isEqualTo("Wanderers");
    }
}