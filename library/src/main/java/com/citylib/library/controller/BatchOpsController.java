package com.citylib.library.controller;

import com.citylib.library.model.RentedCopy;
import com.citylib.library.repository.RentedCopyRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/library/batchOps")
public class BatchOpsController {

    @Autowired
    RentedCopyRepository rentedCopyRepository;
    @Value("${config.options.rentTime}")
    private int rentTime;

    @GetMapping("/pastDueDate")
    public ResponseEntity<List<RentedCopy>> getPastDueDate() {
        LocalDate nonExtendedDate = LocalDate.now().minusDays(rentTime + 1);
        LocalDate extendedDate = LocalDate.now().minusDays((rentTime +1) * 2);

        List<RentedCopy> dueLateList = rentedCopyRepository.findPastDueDate(nonExtendedDate, false);
        dueLateList.addAll(rentedCopyRepository.findPastDueDate(extendedDate, true));

        return new ResponseEntity<>(dueLateList, HttpStatus.OK);
    }
}
