package com.citylib.library.controller;

import com.citylib.library.model.BookCopy;
import com.citylib.library.model.RentedCopy;
import com.citylib.library.repository.LibraryRepository;
import com.citylib.library.repository.RentedCopyRepository;
import com.citylib.library.service.BookCopyService;
import com.citylib.library.service.RentedCopyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/library")
public class DefaultController {

    @Autowired
    RentedCopyService rentedCopyService;

    @Autowired
    BookCopyService bookCopyService;

    @Autowired
    LibraryRepository libraryRepository;

    @Autowired
    RentedCopyRepository rentedCopyRepository;

    Logger logger = LoggerFactory.getLogger(DefaultController.class);

    @GetMapping(value = "copyListByBook/{id}", produces = "application/json")
    public ResponseEntity getBookCopyList(@PathVariable("id") long id) {
        return new ResponseEntity<>(bookCopyService.findAllCopiesByBookId(id), HttpStatus.OK);
    }

    @GetMapping(value = "rentedCopiesList", produces = "application/json")
    public ResponseEntity getRentedCopiesList(@RequestParam("userId") long userId) {
        return new ResponseEntity<>(rentedCopyRepository.findAllByUserId(userId), HttpStatus.OK);
    }

    @PostMapping(value = "rentCopy", produces = "application/json")
    public ResponseEntity putRentCopy(
            @RequestParam("userId") long userId,
            @RequestParam("bookId") long bookId,
            @RequestParam("libraryId") long libraryId
            ) {
        RentedCopy rentedCopy = new RentedCopy();

        List<BookCopy> availableCopy = bookCopyService.findAvailableCopy(bookId, libraryId);
        if(availableCopy.size() > 0) {
            rentedCopy.setBookCopy(availableCopy.get(0));
            rentedCopy.setUserId(userId);
            rentedCopy.setStartDate(LocalDate.now());
            rentedCopy.setExtended(false);
            rentedCopy.setReturned(false);
            RentedCopy result = rentedCopyService.save(rentedCopy);
            logger.info("New rented copy was added with id " + rentedCopy.getId());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>("{ error: 'No copies available.' }", HttpStatus.OK);
        }
    }

    @PutMapping(value = "returnCopy", produces = "application/json")
    public ResponseEntity returnCopy(
            @RequestParam("rentedCopyId") long rentedCopyId,
            @RequestParam("userId") long userId
            ) {
        RentedCopy rentedCopy = rentedCopyRepository.findById(rentedCopyId);
        if(rentedCopy.getUserId() == userId && !rentedCopy.isReturned()) {
            rentedCopy.setReturned(true);
            rentedCopyRepository.save(rentedCopy);
            logger.info("Rented copy with id " + rentedCopy.getId() + " has been marked as returned");
            return new ResponseEntity<>("", HttpStatus.OK);
        }
        if(rentedCopy.getUserId() == userId && rentedCopy.isReturned()) {
            logger.info("Rented copy with id " + rentedCopy.getId() + " has already been returned");
            return new ResponseEntity<>("{error: 'Instance of rented copy has already been returned'}", HttpStatus.UNPROCESSABLE_ENTITY);
        }
        logger.info("Rented copy with given elements does not exist");
        return new ResponseEntity<>("{error: 'Rented copy elements do not match'}", HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @GetMapping(value = "extendRentedCopy", produces = "application/json")
    public ResponseEntity extendRentedCopy(@RequestParam("rentedCopyId") long rentedCopyId) {
        RentedCopy rentedCopy = rentedCopyRepository.findById(rentedCopyId);
        if(rentedCopy.isExtended()) {
            return new ResponseEntity<>("{error: 'Rented copy is already extended'}", HttpStatus.UNPROCESSABLE_ENTITY);
        }
        rentedCopy.setExtended(true);
        rentedCopyRepository.save(rentedCopy);
        return new ResponseEntity<>("true", HttpStatus.OK);
    }

    @GetMapping(value = "libraryList", produces = "application/json")
    public ResponseEntity getLibraryList() {
        return new ResponseEntity<>(libraryRepository.findAll(), HttpStatus.OK);
    }

}
