package com.citylib.library.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class LibraryWithCopies {
    private long id;
    private String name;
    private int copyCount;
    private int availableCount;
}
