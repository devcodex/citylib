package com.citylib.library.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class BookCopy {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private long bookId;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    private Library library;
}
