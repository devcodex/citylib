package com.citylib.library.repository;

import com.citylib.library.model.BookCopy;
import com.citylib.library.model.Library;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BookCopyRepository extends JpaRepository<BookCopy, Long> {
    List<BookCopy> findAllByLibraryAndBookId(Library library, long bookId);
    List<BookCopy> findAllByBookIdOrderByLibrary(long id);
    int countByLibraryIdAndBookId(long libraryId, long bookId);

    BookCopy findById(long id);
}
