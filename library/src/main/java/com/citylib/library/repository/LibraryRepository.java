package com.citylib.library.repository;

import com.citylib.library.model.Library;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LibraryRepository extends JpaRepository<Library, Long> {
    Library findById(long id);
}
