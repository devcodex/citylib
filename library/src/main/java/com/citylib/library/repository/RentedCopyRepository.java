package com.citylib.library.repository;

import com.citylib.library.model.BookCopy;
import com.citylib.library.model.Library;
import com.citylib.library.model.RentedCopy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;

public interface RentedCopyRepository extends JpaRepository<RentedCopy, Long> {
    int countByBookCopyAndReturned(BookCopy bookCopy, boolean returned);

    List<RentedCopy> findAllByStartDateAndExtendedAndReturned(LocalDate date, boolean extended, boolean returned);

    @Query("select rc from RentedCopy rc where start_date < :date and extended = :extended and returned = false")
    List<RentedCopy> findPastDueDate(LocalDate date, boolean extended);

    List<RentedCopy> findAllByUserId(long userId);

    RentedCopy findById(long rentedCopyId);
}
