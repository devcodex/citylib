package com.citylib.library.service;

import com.citylib.library.dto.LibraryWithCopies;
import com.citylib.library.model.BookCopy;
import com.citylib.library.model.Library;
import com.citylib.library.model.RentedCopy;
import com.citylib.library.repository.BookCopyRepository;
import com.citylib.library.repository.LibraryRepository;
import com.citylib.library.repository.RentedCopyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RentedCopyService {
    @Autowired
    BookCopyRepository bookCopyRepository;
    @Autowired
    LibraryRepository libraryRepository;
    @Autowired
    RentedCopyRepository rentedCopyRepository;

    public RentedCopy save(RentedCopy rentedCopy) {
        return rentedCopyRepository.save(rentedCopy);
    }
}
