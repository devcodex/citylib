insert into address(id, street, city, postal_code) values (1, '12 Rue de NulPart', 'Villeurbanne', '69100');
insert into address(id, street, city, postal_code) values (2, '25 Rue Perdu', 'Lyon', '69000');
insert into address(id, street, city, postal_code) values (3, '80 Route du Progres', 'Villeurbanne', '69100');

insert into library_schedule(id, monday_open, monday_close, tuesday_open, tuesday_close, wednesday_open, wednesday_close, thursday_open, thursday_close, friday_open, friday_close, saturday_open, saturday_close, sunday_open, sunday_close) values(1, '09:00', '19:00','09:00', '19:00','09:00', '19:00','09:00', '19:00','09:00', '19:00','13:00', '18:00','09:00', '12:00');
insert into library_schedule(id, monday_open, monday_close, tuesday_open, tuesday_close, wednesday_open, wednesday_close, thursday_open, thursday_close, friday_open, friday_close, saturday_open, saturday_close, sunday_open, sunday_close) values(2, '09:00', '19:00','09:00', '19:00','09:00', '19:00','09:00', '19:00','09:00', '19:00','10:00', '18:00','----', '----');
insert into library_schedule(id, monday_open, monday_close, tuesday_open, tuesday_close, wednesday_open, wednesday_close, thursday_open, thursday_close, friday_open, friday_close, saturday_open, saturday_close, sunday_open, sunday_close) values(3, '09:00', '19:00','09:00', '19:00','09:00', '19:00','09:00', '19:00','09:00', '19:00','10:00', '15:00','10:00', '13:00');

insert into library(id, address_id, name, library_schedule_id) values (1, 1, 'Pave Biblio', 1);
insert into library(id, address_id, name, library_schedule_id) values (2, 2, 'LibDemain', 2);
insert into library(id, address_id, name, library_schedule_id) values (3, 3, 'Biblioteque Centre Ville', 3);

insert into book_copy (id, book_id, library_id) values (1, 45, 3);
insert into book_copy (id, book_id, library_id) values (2, 1, 1);
insert into book_copy (id, book_id, library_id) values (3, 10, 3);
insert into book_copy (id, book_id, library_id) values (4, 20, 2);
insert into book_copy (id, book_id, library_id) values (5, 2, 1);
insert into book_copy (id, book_id, library_id) values (6, 44, 1);
insert into book_copy (id, book_id, library_id) values (7, 46, 1);
insert into book_copy (id, book_id, library_id) values (8, 49, 3);
insert into book_copy (id, book_id, library_id) values (9, 10, 1);
insert into book_copy (id, book_id, library_id) values (10, 2, 3);
insert into book_copy (id, book_id, library_id) values (11, 10, 3);
insert into book_copy (id, book_id, library_id) values (12, 28, 2);
insert into book_copy (id, book_id, library_id) values (13, 11, 1);
insert into book_copy (id, book_id, library_id) values (14, 49, 3);
insert into book_copy (id, book_id, library_id) values (15, 14, 2);
insert into book_copy (id, book_id, library_id) values (16, 8, 1);
insert into book_copy (id, book_id, library_id) values (17, 50, 2);
insert into book_copy (id, book_id, library_id) values (18, 21, 1);
insert into book_copy (id, book_id, library_id) values (19, 36, 2);
insert into book_copy (id, book_id, library_id) values (20, 2, 2);
insert into book_copy (id, book_id, library_id) values (21, 34, 3);
insert into book_copy (id, book_id, library_id) values (22, 26, 2);
insert into book_copy (id, book_id, library_id) values (23, 36, 1);
insert into book_copy (id, book_id, library_id) values (24, 43, 3);
insert into book_copy (id, book_id, library_id) values (25, 10, 1);
insert into book_copy (id, book_id, library_id) values (26, 5, 2);
insert into book_copy (id, book_id, library_id) values (27, 22, 1);
insert into book_copy (id, book_id, library_id) values (28, 48, 3);
insert into book_copy (id, book_id, library_id) values (29, 44, 2);
insert into book_copy (id, book_id, library_id) values (30, 34, 3);
insert into book_copy (id, book_id, library_id) values (31, 46, 1);
insert into book_copy (id, book_id, library_id) values (32, 45, 1);
insert into book_copy (id, book_id, library_id) values (33, 35, 1);
insert into book_copy (id, book_id, library_id) values (34, 43, 3);
insert into book_copy (id, book_id, library_id) values (35, 40, 1);
insert into book_copy (id, book_id, library_id) values (36, 30, 1);
insert into book_copy (id, book_id, library_id) values (37, 16, 2);
insert into book_copy (id, book_id, library_id) values (38, 20, 1);
insert into book_copy (id, book_id, library_id) values (39, 16, 1);
insert into book_copy (id, book_id, library_id) values (40, 31, 3);
insert into book_copy (id, book_id, library_id) values (41, 10, 1);
insert into book_copy (id, book_id, library_id) values (42, 12, 2);
insert into book_copy (id, book_id, library_id) values (43, 2, 1);
insert into book_copy (id, book_id, library_id) values (44, 44, 1);
insert into book_copy (id, book_id, library_id) values (45, 10, 2);
insert into book_copy (id, book_id, library_id) values (46, 39, 3);
insert into book_copy (id, book_id, library_id) values (47, 40, 2);
insert into book_copy (id, book_id, library_id) values (48, 29, 3);
insert into book_copy (id, book_id, library_id) values (49, 39, 1);
insert into book_copy (id, book_id, library_id) values (50, 10, 3);
insert into book_copy (id, book_id, library_id) values (51, 40, 2);
insert into book_copy (id, book_id, library_id) values (52, 21, 2);
insert into book_copy (id, book_id, library_id) values (53, 13, 1);
insert into book_copy (id, book_id, library_id) values (54, 31, 3);
insert into book_copy (id, book_id, library_id) values (55, 47, 3);
insert into book_copy (id, book_id, library_id) values (56, 19, 2);
insert into book_copy (id, book_id, library_id) values (57, 6, 1);
insert into book_copy (id, book_id, library_id) values (58, 28, 1);
insert into book_copy (id, book_id, library_id) values (59, 32, 2);
insert into book_copy (id, book_id, library_id) values (60, 49, 1);
insert into book_copy (id, book_id, library_id) values (61, 20, 2);
insert into book_copy (id, book_id, library_id) values (62, 49, 3);
insert into book_copy (id, book_id, library_id) values (63, 29, 1);
insert into book_copy (id, book_id, library_id) values (64, 40, 1);
insert into book_copy (id, book_id, library_id) values (65, 13, 3);
insert into book_copy (id, book_id, library_id) values (66, 40, 2);
insert into book_copy (id, book_id, library_id) values (67, 21, 2);
insert into book_copy (id, book_id, library_id) values (68, 1, 2);
insert into book_copy (id, book_id, library_id) values (69, 40, 3);
insert into book_copy (id, book_id, library_id) values (70, 13, 1);
insert into book_copy (id, book_id, library_id) values (71, 24, 1);
insert into book_copy (id, book_id, library_id) values (72, 17, 3);
insert into book_copy (id, book_id, library_id) values (73, 4, 2);
insert into book_copy (id, book_id, library_id) values (74, 44, 1);
insert into book_copy (id, book_id, library_id) values (75, 42, 1);
insert into book_copy (id, book_id, library_id) values (76, 2, 1);
insert into book_copy (id, book_id, library_id) values (77, 36, 2);
insert into book_copy (id, book_id, library_id) values (78, 46, 1);
insert into book_copy (id, book_id, library_id) values (79, 37, 1);
insert into book_copy (id, book_id, library_id) values (80, 40, 1);
insert into book_copy (id, book_id, library_id) values (81, 38, 3);
insert into book_copy (id, book_id, library_id) values (82, 29, 2);
insert into book_copy (id, book_id, library_id) values (83, 12, 2);
insert into book_copy (id, book_id, library_id) values (84, 50, 3);
insert into book_copy (id, book_id, library_id) values (85, 36, 1);
insert into book_copy (id, book_id, library_id) values (86, 34, 1);
insert into book_copy (id, book_id, library_id) values (87, 41, 1);
insert into book_copy (id, book_id, library_id) values (88, 7, 3);
insert into book_copy (id, book_id, library_id) values (89, 21, 1);
insert into book_copy (id, book_id, library_id) values (90, 20, 1);
insert into book_copy (id, book_id, library_id) values (91, 11, 3);
insert into book_copy (id, book_id, library_id) values (92, 5, 3);
insert into book_copy (id, book_id, library_id) values (93, 32, 3);
insert into book_copy (id, book_id, library_id) values (94, 47, 1);
insert into book_copy (id, book_id, library_id) values (95, 26, 1);
insert into book_copy (id, book_id, library_id) values (96, 27, 3);
insert into book_copy (id, book_id, library_id) values (97, 7, 3);
insert into book_copy (id, book_id, library_id) values (98, 47, 1);
insert into book_copy (id, book_id, library_id) values (99, 23, 3);
insert into book_copy (id, book_id, library_id) values (100, 33, 2);
insert into book_copy (id, book_id, library_id) values (101, 23, 2);
insert into book_copy (id, book_id, library_id) values (102, 45, 2);
insert into book_copy (id, book_id, library_id) values (103, 13, 1);
insert into book_copy (id, book_id, library_id) values (104, 46, 3);
insert into book_copy (id, book_id, library_id) values (105, 28, 3);
insert into book_copy (id, book_id, library_id) values (106, 29, 1);
insert into book_copy (id, book_id, library_id) values (107, 14, 3);
insert into book_copy (id, book_id, library_id) values (108, 11, 3);
insert into book_copy (id, book_id, library_id) values (109, 42, 3);
insert into book_copy (id, book_id, library_id) values (110, 14, 3);
insert into book_copy (id, book_id, library_id) values (111, 9, 2);
insert into book_copy (id, book_id, library_id) values (112, 42, 1);
insert into book_copy (id, book_id, library_id) values (113, 11, 1);
insert into book_copy (id, book_id, library_id) values (114, 27, 3);
insert into book_copy (id, book_id, library_id) values (115, 3, 3);
insert into book_copy (id, book_id, library_id) values (116, 27, 2);
insert into book_copy (id, book_id, library_id) values (117, 10, 3);
insert into book_copy (id, book_id, library_id) values (118, 21, 1);
insert into book_copy (id, book_id, library_id) values (119, 50, 1);
insert into book_copy (id, book_id, library_id) values (120, 40, 3);
insert into book_copy (id, book_id, library_id) values (121, 3, 2);
insert into book_copy (id, book_id, library_id) values (122, 17, 2);
insert into book_copy (id, book_id, library_id) values (123, 20, 3);
insert into book_copy (id, book_id, library_id) values (124, 20, 1);
insert into book_copy (id, book_id, library_id) values (125, 25, 3);
insert into book_copy (id, book_id, library_id) values (126, 11, 1);
insert into book_copy (id, book_id, library_id) values (127, 47, 3);
insert into book_copy (id, book_id, library_id) values (128, 1, 2);
insert into book_copy (id, book_id, library_id) values (129, 36, 1);
insert into book_copy (id, book_id, library_id) values (130, 7, 3);
insert into book_copy (id, book_id, library_id) values (131, 22, 3);
insert into book_copy (id, book_id, library_id) values (132, 39, 1);
insert into book_copy (id, book_id, library_id) values (133, 12, 1);
insert into book_copy (id, book_id, library_id) values (134, 40, 1);
insert into book_copy (id, book_id, library_id) values (135, 47, 3);
insert into book_copy (id, book_id, library_id) values (136, 43, 3);
insert into book_copy (id, book_id, library_id) values (137, 10, 2);
insert into book_copy (id, book_id, library_id) values (138, 34, 3);
insert into book_copy (id, book_id, library_id) values (139, 23, 1);
insert into book_copy (id, book_id, library_id) values (140, 11, 2);
insert into book_copy (id, book_id, library_id) values (141, 22, 3);
insert into book_copy (id, book_id, library_id) values (142, 19, 3);
insert into book_copy (id, book_id, library_id) values (143, 40, 3);
insert into book_copy (id, book_id, library_id) values (144, 33, 2);
insert into book_copy (id, book_id, library_id) values (145, 14, 1);
insert into book_copy (id, book_id, library_id) values (146, 43, 2);
insert into book_copy (id, book_id, library_id) values (147, 30, 2);
insert into book_copy (id, book_id, library_id) values (148, 41, 2);
insert into book_copy (id, book_id, library_id) values (149, 7, 3);
insert into book_copy (id, book_id, library_id) values (150, 48, 3);
insert into book_copy (id, book_id, library_id) values (151, 30, 3);
insert into book_copy (id, book_id, library_id) values (152, 37, 3);
insert into book_copy (id, book_id, library_id) values (153, 48, 2);
insert into book_copy (id, book_id, library_id) values (154, 49, 2);
insert into book_copy (id, book_id, library_id) values (155, 31, 2);
insert into book_copy (id, book_id, library_id) values (156, 16, 2);
insert into book_copy (id, book_id, library_id) values (157, 37, 3);
insert into book_copy (id, book_id, library_id) values (158, 18, 3);
insert into book_copy (id, book_id, library_id) values (159, 2, 1);
insert into book_copy (id, book_id, library_id) values (160, 17, 3);
insert into book_copy (id, book_id, library_id) values (161, 16, 2);
insert into book_copy (id, book_id, library_id) values (162, 41, 2);
insert into book_copy (id, book_id, library_id) values (163, 33, 2);
insert into book_copy (id, book_id, library_id) values (164, 49, 1);
insert into book_copy (id, book_id, library_id) values (165, 2, 2);
insert into book_copy (id, book_id, library_id) values (166, 23, 3);
insert into book_copy (id, book_id, library_id) values (167, 34, 2);
insert into book_copy (id, book_id, library_id) values (168, 12, 2);
insert into book_copy (id, book_id, library_id) values (169, 26, 3);
insert into book_copy (id, book_id, library_id) values (170, 48, 1);
insert into book_copy (id, book_id, library_id) values (171, 20, 2);
insert into book_copy (id, book_id, library_id) values (172, 12, 1);
insert into book_copy (id, book_id, library_id) values (173, 7, 1);
insert into book_copy (id, book_id, library_id) values (174, 49, 2);
insert into book_copy (id, book_id, library_id) values (175, 13, 1);
insert into book_copy (id, book_id, library_id) values (176, 17, 1);
insert into book_copy (id, book_id, library_id) values (177, 13, 3);
insert into book_copy (id, book_id, library_id) values (178, 16, 3);
insert into book_copy (id, book_id, library_id) values (179, 44, 3);
insert into book_copy (id, book_id, library_id) values (180, 22, 2);
insert into book_copy (id, book_id, library_id) values (181, 48, 1);
insert into book_copy (id, book_id, library_id) values (182, 2, 1);
insert into book_copy (id, book_id, library_id) values (183, 19, 3);
insert into book_copy (id, book_id, library_id) values (184, 50, 1);
insert into book_copy (id, book_id, library_id) values (185, 44, 2);
insert into book_copy (id, book_id, library_id) values (186, 16, 2);
insert into book_copy (id, book_id, library_id) values (187, 19, 3);
insert into book_copy (id, book_id, library_id) values (188, 23, 2);
insert into book_copy (id, book_id, library_id) values (189, 12, 1);
insert into book_copy (id, book_id, library_id) values (190, 28, 1);
insert into book_copy (id, book_id, library_id) values (191, 3, 2);
insert into book_copy (id, book_id, library_id) values (192, 3, 1);
insert into book_copy (id, book_id, library_id) values (193, 21, 1);
insert into book_copy (id, book_id, library_id) values (194, 8, 2);
insert into book_copy (id, book_id, library_id) values (195, 23, 2);
insert into book_copy (id, book_id, library_id) values (196, 25, 2);
insert into book_copy (id, book_id, library_id) values (197, 23, 1);
insert into book_copy (id, book_id, library_id) values (198, 23, 2);
insert into book_copy (id, book_id, library_id) values (199, 8, 2);
insert into book_copy (id, book_id, library_id) values (200, 16, 2);
insert into book_copy (id, book_id, library_id) values (201, 22, 3);
insert into book_copy (id, book_id, library_id) values (202, 19, 1);
insert into book_copy (id, book_id, library_id) values (203, 16, 2);
insert into book_copy (id, book_id, library_id) values (204, 6, 3);
insert into book_copy (id, book_id, library_id) values (205, 7, 1);
insert into book_copy (id, book_id, library_id) values (206, 29, 2);
insert into book_copy (id, book_id, library_id) values (207, 29, 1);
insert into book_copy (id, book_id, library_id) values (208, 3, 1);
insert into book_copy (id, book_id, library_id) values (209, 47, 2);
insert into book_copy (id, book_id, library_id) values (210, 40, 2);
insert into book_copy (id, book_id, library_id) values (211, 38, 3);
insert into book_copy (id, book_id, library_id) values (212, 29, 3);
insert into book_copy (id, book_id, library_id) values (213, 44, 1);
insert into book_copy (id, book_id, library_id) values (214, 42, 1);
insert into book_copy (id, book_id, library_id) values (215, 25, 2);
insert into book_copy (id, book_id, library_id) values (216, 1, 2);
insert into book_copy (id, book_id, library_id) values (217, 37, 3);
insert into book_copy (id, book_id, library_id) values (218, 34, 1);
insert into book_copy (id, book_id, library_id) values (219, 39, 3);
insert into book_copy (id, book_id, library_id) values (220, 8, 1);
insert into book_copy (id, book_id, library_id) values (221, 46, 1);
insert into book_copy (id, book_id, library_id) values (222, 19, 1);
insert into book_copy (id, book_id, library_id) values (223, 31, 2);
insert into book_copy (id, book_id, library_id) values (224, 8, 3);
insert into book_copy (id, book_id, library_id) values (225, 38, 2);
insert into book_copy (id, book_id, library_id) values (226, 25, 1);
insert into book_copy (id, book_id, library_id) values (227, 32, 3);
insert into book_copy (id, book_id, library_id) values (228, 8, 3);
insert into book_copy (id, book_id, library_id) values (229, 27, 3);
insert into book_copy (id, book_id, library_id) values (230, 13, 3);
insert into book_copy (id, book_id, library_id) values (231, 30, 1);
insert into book_copy (id, book_id, library_id) values (232, 39, 2);
insert into book_copy (id, book_id, library_id) values (233, 33, 2);
insert into book_copy (id, book_id, library_id) values (234, 48, 3);
insert into book_copy (id, book_id, library_id) values (235, 18, 2);
insert into book_copy (id, book_id, library_id) values (236, 44, 1);
insert into book_copy (id, book_id, library_id) values (237, 43, 3);
insert into book_copy (id, book_id, library_id) values (238, 43, 2);
insert into book_copy (id, book_id, library_id) values (239, 47, 3);
insert into book_copy (id, book_id, library_id) values (240, 11, 2);
insert into book_copy (id, book_id, library_id) values (241, 48, 3);
insert into book_copy (id, book_id, library_id) values (242, 50, 2);
insert into book_copy (id, book_id, library_id) values (243, 5, 2);
insert into book_copy (id, book_id, library_id) values (244, 18, 2);
insert into book_copy (id, book_id, library_id) values (245, 24, 2);
insert into book_copy (id, book_id, library_id) values (246, 28, 1);
insert into book_copy (id, book_id, library_id) values (247, 41, 1);
insert into book_copy (id, book_id, library_id) values (248, 47, 1);
insert into book_copy (id, book_id, library_id) values (249, 40, 3);
insert into book_copy (id, book_id, library_id) values (250, 10, 1);

insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (1, true, false, to_date('23/8/2019', 'DD/MM/YYYY'), 6, 241);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (2, true, true, to_date('27/9/2019', 'DD/MM/YYYY'), 4, 102);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (3, false, false, to_date('2/9/2019', 'DD/MM/YYYY'), 4, 13);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (4, true, false, to_date('11/8/2019', 'DD/MM/YYYY'), 4, 117);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (5, false, false, to_date('31/8/2019', 'DD/MM/YYYY'), 6, 55);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (6, false, true, to_date('7/8/2019', 'DD/MM/YYYY'), 6, 165);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (7, false, true, to_date('20/8/2019', 'DD/MM/YYYY'), 6, 80);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (8, false, false, to_date('22/8/2019', 'DD/MM/YYYY'), 3, 77);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (9, true, true, to_date('24/10/2019', 'DD/MM/YYYY'), 4, 52);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (10, true, true, to_date('19/9/2019', 'DD/MM/YYYY'), 5, 119);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (11, false, false, to_date('30/9/2019', 'DD/MM/YYYY'), 5, 180);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (12, false, true, to_date('19/9/2019', 'DD/MM/YYYY'), 4, 68);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (13, true, false, to_date('24/10/2019', 'DD/MM/YYYY'), 4, 107);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (14, false, true, to_date('4/8/2019', 'DD/MM/YYYY'), 2, 135);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (15, true, false, to_date('17/10/2019', 'DD/MM/YYYY'), 4, 103);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (16, false, true, to_date('3/9/2019', 'DD/MM/YYYY'), 6, 57);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (17, false, false, to_date('24/9/2019', 'DD/MM/YYYY'), 2, 105);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (18, false, true, to_date('18/8/2019', 'DD/MM/YYYY'), 2, 72);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (19, false, false, to_date('18/9/2019', 'DD/MM/YYYY'), 4, 33);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (20, false, true, to_date('9/9/2019', 'DD/MM/YYYY'), 2, 166);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (21, true, true, to_date('24/10/2019', 'DD/MM/YYYY'), 2, 172);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (22, false, true, to_date('16/8/2019', 'DD/MM/YYYY'), 3, 124);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (23, false, false, to_date('10/9/2019', 'DD/MM/YYYY'), 4, 158);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (24, false, true, to_date('28/8/2019', 'DD/MM/YYYY'), 4, 67);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (25, true, true, to_date('1/10/2019', 'DD/MM/YYYY'), 3, 88);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (26, true, false, to_date('19/9/2019', 'DD/MM/YYYY'), 3, 127);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (27, true, false, to_date('13/9/2019', 'DD/MM/YYYY'), 2, 176);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (28, false, true, to_date('1/8/2019', 'DD/MM/YYYY'), 3, 5);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (29, true, true, to_date('4/9/2019', 'DD/MM/YYYY'), 3, 93);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (30, false, false, to_date('27/9/2019', 'DD/MM/YYYY'), 4, 66);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (31, false, false, to_date('20/9/2019', 'DD/MM/YYYY'), 4, 188);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (32, true, true, to_date('6/10/2019', 'DD/MM/YYYY'), 5, 247);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (33, false, false, to_date('28/9/2019', 'DD/MM/YYYY'), 3, 224);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (34, false, true, to_date('21/10/2019', 'DD/MM/YYYY'), 5, 169);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (35, false, false, to_date('11/10/2019', 'DD/MM/YYYY'), 6, 132);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (36, true, true, to_date('30/10/2019', 'DD/MM/YYYY'), 4, 166);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (37, true, true, to_date('15/9/2019', 'DD/MM/YYYY'), 3, 18);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (38, false, false, to_date('19/10/2019', 'DD/MM/YYYY'), 5, 20);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (39, true, true, to_date('17/9/2019', 'DD/MM/YYYY'), 6, 208);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (40, true, true, to_date('11/9/2019', 'DD/MM/YYYY'), 5, 33);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (41, false, true, to_date('18/10/2019', 'DD/MM/YYYY'), 2, 98);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (42, true, true, to_date('20/10/2019', 'DD/MM/YYYY'), 5, 226);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (43, false, false, to_date('14/10/2019', 'DD/MM/YYYY'), 3, 119);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (44, false, true, to_date('29/9/2019', 'DD/MM/YYYY'), 2, 34);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (45, false, true, to_date('17/10/2019', 'DD/MM/YYYY'), 5, 240);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (46, true, false, to_date('29/8/2019', 'DD/MM/YYYY'), 5, 16);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (47, true, true, to_date('19/9/2019', 'DD/MM/YYYY'), 5, 176);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (48, false, true, to_date('7/10/2019', 'DD/MM/YYYY'), 2, 19);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (49, false, false, to_date('25/9/2019', 'DD/MM/YYYY'), 6, 139);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (50, false, true, to_date('18/9/2019', 'DD/MM/YYYY'), 2, 136);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (51, true, false, to_date('23/10/2019', 'DD/MM/YYYY'), 6, 170);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (52, false, true, to_date('6/9/2019', 'DD/MM/YYYY'), 5, 247);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (53, true, false, to_date('4/10/2019', 'DD/MM/YYYY'), 2, 155);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (54, true, true, to_date('17/10/2019', 'DD/MM/YYYY'), 4, 95);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (55, true, true, to_date('8/10/2019', 'DD/MM/YYYY'), 3, 171);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (56, true, true, to_date('14/10/2019', 'DD/MM/YYYY'), 5, 98);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (57, true, false, to_date('23/10/2019', 'DD/MM/YYYY'), 3, 14);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (58, false, true, to_date('24/10/2019', 'DD/MM/YYYY'), 2, 191);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (59, true, false, to_date('18/10/2019', 'DD/MM/YYYY'), 2, 76);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (60, true, false, to_date('29/9/2019', 'DD/MM/YYYY'), 2, 75);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (61, true, false, to_date('30/8/2019', 'DD/MM/YYYY'), 2, 182);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (62, false, false, to_date('27/8/2019', 'DD/MM/YYYY'), 3, 35);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (63, false, false, to_date('17/9/2019', 'DD/MM/YYYY'), 5, 51);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (64, false, true, to_date('17/10/2019', 'DD/MM/YYYY'), 2, 7);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (65, false, true, to_date('27/8/2019', 'DD/MM/YYYY'), 5, 168);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (66, false, true, to_date('27/8/2019', 'DD/MM/YYYY'), 6, 169);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (67, false, true, to_date('23/8/2019', 'DD/MM/YYYY'), 3, 7);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (68, true, false, to_date('27/8/2019', 'DD/MM/YYYY'), 4, 206);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (69, true, false, to_date('16/9/2019', 'DD/MM/YYYY'), 3, 249);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (70, true, false, to_date('9/8/2019', 'DD/MM/YYYY'), 6, 237);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (71, true, true, to_date('12/8/2019', 'DD/MM/YYYY'), 4, 31);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (72, true, false, to_date('4/10/2019', 'DD/MM/YYYY'), 3, 94);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (73, false, true, to_date('6/8/2019', 'DD/MM/YYYY'), 2, 246);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (74, true, true, to_date('12/8/2019', 'DD/MM/YYYY'), 3, 9);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (75, false, true, to_date('12/9/2019', 'DD/MM/YYYY'), 6, 6);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (76, true, false, to_date('6/9/2019', 'DD/MM/YYYY'), 5, 123);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (77, false, false, to_date('11/9/2019', 'DD/MM/YYYY'), 4, 28);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (78, true, true, to_date('27/10/2019', 'DD/MM/YYYY'), 4, 120);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (79, true, true, to_date('23/10/2019', 'DD/MM/YYYY'), 3, 127);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (80, false, true, to_date('20/10/2019', 'DD/MM/YYYY'), 6, 3);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (81, false, true, to_date('14/9/2019', 'DD/MM/YYYY'), 2, 82);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (82, false, false, to_date('8/9/2019', 'DD/MM/YYYY'), 2, 21);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (83, false, true, to_date('25/9/2019', 'DD/MM/YYYY'), 5, 166);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (84, false, false, to_date('11/9/2019', 'DD/MM/YYYY'), 3, 99);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (85, true, false, to_date('30/10/2019', 'DD/MM/YYYY'), 2, 239);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (86, false, false, to_date('17/10/2019', 'DD/MM/YYYY'), 5, 56);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (87, true, true, to_date('5/8/2019', 'DD/MM/YYYY'), 3, 48);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (88, true, false, to_date('11/9/2019', 'DD/MM/YYYY'), 2, 171);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (89, false, true, to_date('2/8/2019', 'DD/MM/YYYY'), 6, 245);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (90, false, false, to_date('16/10/2019', 'DD/MM/YYYY'), 6, 192);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (91, true, true, to_date('22/10/2019', 'DD/MM/YYYY'), 2, 80);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (92, false, false, to_date('21/10/2019', 'DD/MM/YYYY'), 6, 71);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (93, true, false, to_date('20/8/2019', 'DD/MM/YYYY'), 5, 149);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (94, false, true, to_date('23/10/2019', 'DD/MM/YYYY'), 4, 32);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (95, false, false, to_date('8/9/2019', 'DD/MM/YYYY'), 3, 11);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (96, false, false, to_date('22/9/2019', 'DD/MM/YYYY'), 5, 82);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (97, true, true, to_date('25/8/2019', 'DD/MM/YYYY'), 4, 127);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (98, false, true, to_date('11/9/2019', 'DD/MM/YYYY'), 3, 184);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (99, true, true, to_date('25/9/2019', 'DD/MM/YYYY'), 5, 36);
insert into rented_copy (id, extended, returned, start_date, user_id, book_copy_id) values (100, false, true, to_date('19/8/2019', 'DD/MM/YYYY'), 2, 162);

SELECT setval('hibernate_sequence', 1001, false);