package com.citylib.webapp.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

@Getter
@PropertySource("classpath:application.properties")
public class ServiceRoutes {
    @Value("${service.book}")
    private String books;

    @Value("${service.library}")
    private String library;

    @Value("${service.emailbatch}")
    private String emailBatch;

    public String getLibraryList() {
        return library + "libraryList";
    }

    public String getRentedCopiesList(long userId) {
        return library + "rentedCopiesList?userId=" + userId;
    }

    public String getExtendRentedCopy(long rentedCopyId) {
        return library + "extendRentedCopy?rentedCopyId=" + rentedCopyId;
    }

    public String getBookById(long id) {
        return books + id;
    }

    public String getCopyListById(long id) {
        return library + "copyListByBook/" + id;
    }

    public String postRentCopy(long userId, long bookId, long libraryId) {
        return library + "rentCopy?userId=" + userId + "&bookId=" + bookId + "&libraryId=" + libraryId;
    }

    public String getAccountConfirmationEmail(String authString, String email) {
        return emailBatch + "confirmAccount?authString=" + authString + "&recipient=" + email;
    }
}
