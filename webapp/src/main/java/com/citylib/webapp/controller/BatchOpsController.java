package com.citylib.webapp.controller;

import com.citylib.webapp.model.User;
import com.citylib.webapp.repository.UserRepository;
import com.citylib.webapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class BatchOpsController {
    @Autowired
    UserRepository userRepository;

    @GetMapping("/{id}")
    public ResponseEntity<User> getUser(@PathVariable("id") long userId) {
        User user = userRepository.findById(userId);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }
}
