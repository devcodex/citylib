package com.citylib.webapp.controller;

import com.citylib.webapp.config.ServiceRoutes;
import com.citylib.webapp.dto.library.Library;
import com.citylib.webapp.model.User;
import com.citylib.webapp.service.UserService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("")
public class DefaultController {
    Logger logger = LoggerFactory.getLogger(DefaultController.class);
    @Autowired
    RestTemplate restTemplate;
    @Autowired
    UserService userService;
    @Autowired
    ServiceRoutes serviceRoutes;

    private ObjectMapper objectMapper = new ObjectMapper();

    @GetMapping("")
    public String getIndex() {
        return "index";
    }

    @GetMapping("/libraries")
    public String getLibraries(Model model) throws IOException {
        ResponseEntity<String> response = restTemplate.getForEntity(serviceRoutes.getLibraryList(), String.class);
        if(response.getStatusCode() == HttpStatus.OK && response.getBody() != null){
            List<Library> libraryList = objectMapper.readValue(response.getBody(), new TypeReference<List<Library>>(){});
            model.addAttribute("libraryList", libraryList);
        } else {
            model.addAttribute("errorRetrieving", true);
        }
        return "libraries";
    }

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public String getLogin(Principal principal) {
        if(principal == null) {
            return "login";
        } else {
            return "index";
        }
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String postLogin(@RequestParam("username") String username, @RequestParam("password") String password, Model model, HttpServletResponse httpServletResponse) throws IOException {
        try {
            String token = userService.signin(username, password);
            Cookie sessionCookie = new Cookie( "Authorization", token);
            sessionCookie.setMaxAge(10080000);
            httpServletResponse.addCookie( sessionCookie );
        } catch (Exception e) {
            model.addAttribute("loginError", true);
            return "login";
        }
        return "redirect:/";
    }

    @GetMapping("register")
    public String getRegister(Principal principal)
    {
        if(principal == null) {
            return "register";
        } else {
            return "index";
        }
    }

    @PostMapping("register")
    public String postRegister(
            @RequestParam("username") String username,
            @RequestParam("email") String email,
            @RequestParam("emailConfirm") String emailConfirm,
            @RequestParam("password") String password,
            @RequestParam("passwordConfirm") String passwordConfirm,
            Model model
    ) {
        boolean error = false;
        if(!email.equals(emailConfirm)) {
            model.addAttribute("emailMismatch", true);
            error = true;
        }
        if(!password.equals(passwordConfirm)){
            model.addAttribute("passwordMismatch", true);
            error = true;
        }
        if(error) {
            model.addAttribute("error", true);
            return "register";
        } else {
            User registeredUser;
            User user = new User();
            user.setUsername(username);
            user.setPassword(password);
            user.setEmail(email);
            user.setDisabled(true);
            user.setCredentialsExpired(false);
            user.setExpired(false);
            user.setLocked(false);
            user.setAuthString(username + "_" + RandomStringUtils.random(20, true, true));
            try {
                registeredUser = userService.signup(user);
                model.addAttribute("username", registeredUser.getUsername());
            } catch (Exception e) {
                model.addAttribute("error", true);
                if(e.getMessage().equals("422 Username is already in use")) {
                    model.addAttribute("usernameError", true);
                } else if(e.getMessage().equals("422 Email is already in use")) {
                    model.addAttribute("emailError", true);
                } else {
                    logger.info("An unknown error was given when a user tried to register");
                    model.addAttribute("unknownError", true);
                }
                return "register";
            }

            String uri = serviceRoutes.getAccountConfirmationEmail(user.getAuthString(), user.getEmail());

            ResponseEntity<String> response = restTemplate.getForEntity(uri, String.class);

            if(response.getStatusCode() != HttpStatus.OK){
                userService.delete(user.getUsername());
                model.addAttribute("error", "ConfirmationEmailError");
                return "register";
            }
            return "postRegister";
        }
    }

    @GetMapping("/accountConfirmation/{authString}")
    public String getAccountConfirmation(@PathVariable("authString") String authString, Model model) {
        String[] authSplit = authString.split("_");
        User user = userService.search(authSplit[0]);
        if(user.getAuthString().equals(authString)) {
            try {
                user.setDisabled(false);
                userService.save(user);
                model.addAttribute("username", user.getUsername());
            } catch (Exception e) {
                return "index";
            }
        }
        return "postConfirmation";
    }

}
