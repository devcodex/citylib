package com.citylib.webapp.controller;

import com.citylib.webapp.config.ServiceRoutes;
import com.citylib.webapp.dto.book.Book;
import com.citylib.webapp.dto.book.RentedCopy;
import com.citylib.webapp.dto.book.UserRentedCopy;
import com.citylib.webapp.model.User;
import com.citylib.webapp.service.UserService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.security.Principal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Controller
@RequestMapping("/users")
public class UserController {

  @Autowired
  RestTemplate restTemplate;
  @Autowired
  private UserService userService;
  @Autowired
  ServiceRoutes serviceRoutes;

  @Value("${config.options.rentTime}")
  private int rentTime;

  Logger logger = LoggerFactory.getLogger(UserController.class);
  private ObjectMapper objectMapper = new ObjectMapper();
  private DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

  @GetMapping("/dashboard")
  @PreAuthorize("hasRole('ROLE_USER')")
  public String getRentedItems(Model model, Principal principal) throws IOException {
    List<UserRentedCopy> userRentedCopyUnreturned = new ArrayList<>();
    List<UserRentedCopy> userRentedCopyReturned = new ArrayList<>();

    // Retrives currently logged in user
    User user = userService.search(principal.getName());

    // Gets list of rented copies from library microservice based on user id
    ResponseEntity<String> response = restTemplate.getForEntity(serviceRoutes.getRentedCopiesList(user.getId()), String.class);

    if(response.getStatusCode() == HttpStatus.OK && response.getBody() != null){
      List<RentedCopy> rentedCopyList = objectMapper.readValue(response.getBody(), new TypeReference<List<RentedCopy>>(){});
      if(rentedCopyList.size() > 0) {

        // Treats each rented copy
        for (RentedCopy rentedCopy : rentedCopyList) {
          UserRentedCopy userRentedCopy = new UserRentedCopy();

          // Retrieves the book information of current rented copy
          ResponseEntity<String> responseBook = restTemplate.getForEntity(serviceRoutes.getBookById(rentedCopy.getBookCopy().getBookId()), String.class);
          userRentedCopy.setBook(objectMapper.readValue(responseBook.getBody(), Book.class));
          userRentedCopy.setRentedCopy(rentedCopy);

          // Sets whether the user has the ability to extend the rent duration of the current copy
          if (rentedCopy.isExtended() || !getExtendable(rentedCopy.getStartDate())) {
            userRentedCopy.setExtendable(false);
          } else {
            userRentedCopy.setExtendable(true);
          }

          // Sets whether the currenty copy has passed return date or not
          userRentedCopy.setPastDueDate(isPastDueDate(userRentedCopy.getRentedCopy().getStartDate(), userRentedCopy.getRentedCopy().isExtended()));

          LocalDate returnDate = LocalDate.parse(rentedCopy.getStartDate(), timeFormatter);

          // Sets the date the copy should be returned
          if(rentedCopy.isExtended()) {
            userRentedCopy.setReturnDate(returnDate.plusDays(rentTime * 2).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
          } else {
            userRentedCopy.setReturnDate(returnDate.plusDays(rentTime).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
          }

          // Adds copy to returned or unreturned list depending on it's return state
          if(rentedCopy.isReturned()) {
            userRentedCopyReturned.add(userRentedCopy);
          } else {
            userRentedCopyUnreturned.add(userRentedCopy);
          }
        }

        // Orders both lists by return date, and then reverses the order so that it's descending (default is ascending)
        Collections.sort(userRentedCopyReturned, Comparator.comparing(item -> LocalDate.parse(item.getReturnDate(), DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
        Collections.sort(userRentedCopyUnreturned, Comparator.comparing(item -> LocalDate.parse(item.getReturnDate(), DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
        Collections.reverse(userRentedCopyReturned);
        Collections.reverse(userRentedCopyUnreturned);

        model.addAttribute("unreturnedList", userRentedCopyUnreturned);
        model.addAttribute("returnedList", userRentedCopyReturned);
      }
    } else {
      model.addAttribute("errorRetrieving", true);
    }
    model.addAttribute("userData", user);
    return "users/dashboard";
  }

  @GetMapping("/extendRentedCopy/{rentedCopyId}")
  @PreAuthorize("hasRole('ROLE_USER')")
  public String getExtendRentedCopy(@PathVariable("rentedCopyId") long rentedCopyId, RedirectAttributes redirectAttributes) throws IOException {
    ResponseEntity<String> response = restTemplate.getForEntity(serviceRoutes.getExtendRentedCopy(rentedCopyId), String.class);
    if(response.getBody().equals("true")) {
      redirectAttributes.addFlashAttribute("extendStatus", true);
      logger.info("Rented copy " + rentedCopyId + " return time increased by " + rentTime + " days.");
    }
    return "redirect:/users/dashboard";
  }

  public boolean getExtendable(String date) {
    if(LocalDate.parse(date, timeFormatter).plusDays(rentTime).isBefore(LocalDate.now())) {
      return false;
    }
    return true;
  }

  public boolean isPastDueDate(String date, boolean extended) {
    if (extended) {
      return LocalDate.parse(date, timeFormatter).plusDays(rentTime * 2).isBefore(LocalDate.now());
    } else {
      return LocalDate.parse(date, timeFormatter).plusDays(rentTime).isBefore(LocalDate.now());
    }
  }

}
