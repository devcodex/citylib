package com.citylib.webapp.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class BookCopy {
    private long id;
    private String name;
    private String referenceNum;
}
