package com.citylib.webapp.dto.book;

import lombok.Data;

import java.util.Date;

@Data
public class Book {
    private long id;

    private String title;

    private String description;

    private int availableQty;

    private String releaseDate;

    private Author author;

    private Publisher publisher;
}
