package com.citylib.webapp.dto.book;

import lombok.Data;

@Data
public class BookCopy {
    private long id;
    private long bookId;
}
