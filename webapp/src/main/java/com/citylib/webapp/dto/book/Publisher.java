package com.citylib.webapp.dto.book;

import lombok.Data;

@Data
public class Publisher {
    //Maison d'edition
    private long id;

    private String name;
}
