package com.citylib.webapp.dto.book;

import lombok.Data;

@Data
public class UserRentedCopy {
    private RentedCopy rentedCopy;
    private Book book;
    private boolean extendable;
    private String returnDate;
    private boolean pastDueDate;
}
