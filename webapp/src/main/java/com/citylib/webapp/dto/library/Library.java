package com.citylib.webapp.dto.library;

import com.citylib.webapp.dto.misc.Address;
import lombok.Data;

@Data
public class Library {
    private long id;
    private String name;
    private Address address;
    private LibrarySchedule librarySchedule;
}
