package com.citylib.webapp.dto.library;

import lombok.Data;

@Data
public class LibrarySchedule {
    private long id;

    private String mondayOpen;
    private String mondayClose;

    private String tuesdayOpen;
    private String tuesdayClose;

    private String wednesdayOpen;
    private String wednesdayClose;

    private String thursdayOpen;
    private String thursdayClose;

    private String fridayOpen;
    private String fridayClose;

    private String saturdayOpen;
    private String saturdayClose;

    private String sundayOpen;
    private String sundayClose;
}
