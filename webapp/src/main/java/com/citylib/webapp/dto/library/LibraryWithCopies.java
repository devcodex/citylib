package com.citylib.webapp.dto.library;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class LibraryWithCopies {
    private long id;
    private String name;
    private int copyCount;
    private int availableCount;
}
