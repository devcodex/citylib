package com.citylib.webapp.dto.misc;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class Address {
    private long id;
    private String street;
    private String city;
    private String postalCode;
}
