package com.citylib.webapp.dto.misc;

import lombok.Data;

@Data
public class Paging {
    Integer page;
    Integer size;
    String sort;
    String search;

    public Paging() { }

    public Paging(Integer page, Integer size, String sort, String searchString) {
        this.page = page;
        this.size = size;
        this.sort = sort;
        this.search = searchString;
    }
}
