package com.citylib.webapp.dto.user;

import com.citylib.webapp.model.Role;
import lombok.Data;

import java.util.List;

@Data
public class UserDto {
  private String username;
  private String email;
  private String password;
  private List<Role> roles;
}
