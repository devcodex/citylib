package com.citylib.webapp.repository;

import com.citylib.webapp.model.Role;
import com.citylib.webapp.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

public interface RoleRepository extends JpaRepository<Role, Integer> {
  Role findByName(String name);

}
