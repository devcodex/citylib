package com.citylib.webapp.repository;

import com.citylib.webapp.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

public interface UserRepository extends JpaRepository<User, Integer> {

  User findById(long id);

  boolean existsByUsername(String username);

  boolean existsByEmail(String email);

  User findByUsername(String username);

  @Transactional
  void deleteByUsername(String username);

}
