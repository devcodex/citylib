package com.citylib.webapp.service;

import com.citylib.webapp.model.Role;
import com.citylib.webapp.model.User;
import com.citylib.webapp.repository.RoleRepository;
import com.citylib.webapp.repository.UserRepository;
import com.citylib.webapp.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private RoleRepository roleRepository;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Autowired
  private JwtTokenProvider jwtTokenProvider;

  @Autowired
  private AuthenticationManager authenticationManager;

  public String signin(String username, String password) {
    try {
      authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
      return jwtTokenProvider.createToken(username, userRepository.findByUsername(username).getRoles());
    } catch (AuthenticationException e) {
      throw new HttpClientErrorException(HttpStatus.UNPROCESSABLE_ENTITY, "Invalid username/password supplied");
    }
  }

  public User signup(User user) {
    if(userRepository.existsByUsername(user.getUsername())) {
      throw new HttpClientErrorException(HttpStatus.UNPROCESSABLE_ENTITY, "Username is already in use");
    } else if(userRepository.existsByEmail(user.getEmail())) {
      throw new HttpClientErrorException(HttpStatus.UNPROCESSABLE_ENTITY, "Email is already in use");
    }
    List<Role> roleList = new ArrayList<>();
    roleList.add(roleRepository.findByName("ROLE_USER"));
    user.setRoles(roleList);
    user.setPassword(passwordEncoder.encode(user.getPassword()));
    return userRepository.save(user);
  }

  public void save(User user) {
    userRepository.save(user);
  }

  public void delete(String username) {
    userRepository.deleteByUsername(username);
  }

  public User search(String username) {
    User user = userRepository.findByUsername(username);
    if (user == null) {
      throw new HttpClientErrorException(HttpStatus.NOT_FOUND, "The user doesn't exist");
    }
    return user;
  }

  public User whoami(HttpServletRequest req) {
    return userRepository.findByUsername(jwtTokenProvider.getUsername(jwtTokenProvider.resolveToken(req)));
  }

  public String refresh(String username) {
    return jwtTokenProvider.createToken(username, userRepository.findByUsername(username).getRoles());
  }

}
