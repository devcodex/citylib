insert into role (id, name) values (1, 'ROLE_ADMIN');
insert into role (id, name) values (2, 'ROLE_USER');

insert into users (id, username, email, password, auth_string, expired, locked, credentials_expired, disabled)
    values (1, 'admin1', 'admin1@email.com', '{bcrypt}$2a$10$TQV1cnMDRYhxslaYX6KD9O/tlaFDWMA491XmX2c0mfLH9mUQArDCy', 'admin1_12345678901234567890', false, false, false, false);
insert into users (id, username, email, password, auth_string, expired, locked, credentials_expired, disabled)
    values (2, 'user1', 'citylib_user1@yopmail.com', '{bcrypt}$2a$10$TQV1cnMDRYhxslaYX6KD9O/tlaFDWMA491XmX2c0mfLH9mUQArDCy', 'user1_12345678901234567890', false, false, false, false);
insert into users (id, username, email, password, auth_string, expired, locked, credentials_expired, disabled)
    values (3, 'user2', 'citylib_user2@yopmail.com', '{bcrypt}$2a$10$TQV1cnMDRYhxslaYX6KD9O/tlaFDWMA491XmX2c0mfLH9mUQArDCy', 'user2_12345678901234567890', false, false, false, false);
insert into users (id, username, email, password, auth_string, expired, locked, credentials_expired, disabled)
    values (4, 'user3', 'citylib_user3@yopmail.com', '{bcrypt}$2a$10$TQV1cnMDRYhxslaYX6KD9O/tlaFDWMA491XmX2c0mfLH9mUQArDCy', 'user3_12345678901234567890', false, false, false, false);
insert into users (id, username, email, password, auth_string, expired, locked, credentials_expired, disabled)
    values (5, 'user4', 'citylib_user4@yopmail.com', '{bcrypt}$2a$10$TQV1cnMDRYhxslaYX6KD9O/tlaFDWMA491XmX2c0mfLH9mUQArDCy', 'user4_12345678901234567890', false, false, false, false);
insert into users (id, username, email, password, auth_string, expired, locked, credentials_expired, disabled)
    values (6, 'user5', 'citylib_user5@yopmail.com', '{bcrypt}$2a$10$TQV1cnMDRYhxslaYX6KD9O/tlaFDWMA491XmX2c0mfLH9mUQArDCy', 'user5_12345678901234567890', false, false, false, false);
insert into users (id, username, email, password, auth_string, expired, locked, credentials_expired, disabled)
values (7, 'user6', 'citylib_user6@yopmail.com', '{bcrypt}$2a$10$TQV1cnMDRYhxslaYX6KD9O/tlaFDWMA491XmX2c0mfLH9mUQArDCy', 'user6_12345678901234567890', false, false, false, false);

insert into authorities (user_id, role_id) values (1, 1);
insert into authorities (user_id, role_id) values (1, 2);
insert into authorities (user_id, role_id) values (2, 2);
insert into authorities (user_id, role_id) values (3, 2);
insert into authorities (user_id, role_id) values (4, 2);
insert into authorities (user_id, role_id) values (5, 2);
insert into authorities (user_id, role_id) values (6, 2);

-- Sets the sequence to the last known value post initial insertion
SELECT setval('users_id_seq', 8, false);
SELECT setval('role_id_seq', 3, false);